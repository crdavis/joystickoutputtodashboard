package com.mycompany.joystickoutputtodashboard;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

/**
 *
 * @author Carlton Davis
 */
public class ProcessBuilderJoystickTest {
    String theCmd;
    ProcessBuilderJoystick instance;
    
    @BeforeEach
    void setUp() {
        this.theCmd = "src/main/Cpp/Joystick";
        this.instance = new ProcessBuilderJoystick(theCmd);
    }
    

    /**
     * Test of getTheProcess method, of class ProcessBuilderJoystick.
     */
    @Test
    void testGetTheProcess() throws Exception {
        System.out.println("getTheProcess");
        Process result = this.instance.getTheProcess();
        assertNotNull(result);
    }
    
}
