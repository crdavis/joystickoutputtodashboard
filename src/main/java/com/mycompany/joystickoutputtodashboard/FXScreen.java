package com.mycompany.joystickoutputtodashboard;

import eu.hansolo.tilesfx.Tile.SkinType;
import eu.hansolo.tilesfx.TileBuilder;
import eu.hansolo.tilesfx.Tile.TextSize;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.control.TextArea;
import javafx.scene.control.Label;
import javafx.scene.control.ChoiceBox;

/**
 *
 * @author Carlton Davis
 */

/* Class to Create the GUI with the help of TilesFX library */
public class FXScreen extends HBox {

    //TextArea for TextArea tile containing X axis data
    private TextArea textAreaX_axis;

    //TextArea for TextArea tile containing Y axis data
    private TextArea textAreaY_axis;

    //TextArea for TextArea tile containing Z axis data
    private TextArea textAreaZ_axis;

    //The process from the ProcessBuilder process
    Process theProcess;

    //Stores the ChoiceBox choices
    private String choice1;
    private String choice2;

    //Stores the choice the user selects
    private String theChoice;

    //Variable to determine when to stop the thread 
    private boolean exit;

    //Constructor 
    public FXScreen() throws IOException {

        //Build the screen
        this.buildScreen();

        //The command to execute
        String theCmd = "src/main/Cpp/Joystick";

        //ProcessBuilder object use to run the external command
        var theProcessBuilder = new ProcessBuilderJoystick(theCmd);

        //Get the output from the process
        this.theProcess = theProcessBuilder.getTheProcess();

        //Start the thread to get output from the ProcessBuilder object
        this.getOutputThread();
    }

    //Setup a Logger for the class
    private static final Logger LOGGER = Logger.getLogger(FXScreen.class.getName());

    //Build the screen
    private void buildScreen() throws IOException {

        // Define our local setting (used by the clock)
        var locale = new Locale("en", "CA");

        // Tile with a clock
        var clockTile = TileBuilder.create()
                .skinType(SkinType.CLOCK)
                .prefSize(350, 300)
                .title("Current time")
                .dateVisible(true)
                .locale(locale)
                .running(true)
                .build();

        /* Setup custom tile to contain Choicebox and TextArea displaying
            information about the program.    
         */
        //Create ChoiceBox
        ChoiceBox choiceBox = new ChoiceBox();

        //Add the choices to the ChoiceBox
        choice1 = "Display just last output";
        choice2 = "Display all the output";
        choiceBox.getItems().add(choice1);
        choiceBox.getItems().add(choice2);

        //Select the first ChoiceBox item by default
        choiceBox.getSelectionModel().select(0);
        theChoice = choiceBox.getValue().toString();

        //Label for the choiceBox
        Label cbLabel = new Label("Select your choice:  ");
        cbLabel.setTextFill(Color.WHITE);

        //TextArea to display info about the program.
        TextArea infoTextArea = new TextArea();
        String info = "\n\n\nThis program uses ProcessBuilder\nclass to get"
                + "output from a Cpp\nprogram controlling a joystick and\ndisplay"
                + "the output via a\nTileFX dashboard";
        infoTextArea.setText(info);
        infoTextArea.setEditable(false);

        /*Change the background and the font color of the TextArea
           and make the border of the TextArea transparent
         */
        infoTextArea.setStyle("-fx-control-inner-background: #2A2A2A; "
                + "-fx-text-inner-color: white;"
                + "-fx-text-box-border: transparent;");

        //Layout to contain the ChoiceBox, the label and the TextArea
        VBox vbox = new VBox(cbLabel, choiceBox, infoTextArea);

        //Setup event handler for ChoiceBox
        choiceBox.setOnAction((event) -> {
            theChoice = choiceBox.getValue().toString();
            System.out.println("Your selection: " + choiceBox.getValue());
        });

        //Setup the tile containing the ChoiceBox, label and TextArea
        var choiceboxTile = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(350, 300)
                .textSize(TextSize.BIGGER)
                .graphic(vbox)
                .build();

        //Setup tile with TextArea to display X axis output data
        textAreaX_axis = new TextArea();

        //Make the TextArea non editable
        textAreaX_axis.setEditable(false);

        /*Change the background and the font color of the TextArea
           and make the border of the TextArea transparent
         */
        textAreaX_axis.setStyle("-fx-control-inner-background: #2A2A2A; "
                + "-fx-text-inner-color: white;"
                + "-fx-text-box-border: transparent;");

        //Layout to contain the TextArea
        VBox textAreaVbox_X = new VBox(textAreaX_axis);

        //Setup the tile to display X axis output
        var textAreaTile_X = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(350, 300)
                .textSize(TextSize.BIGGER)
                .title("X axis value of joystick motion")
                .graphic(textAreaVbox_X)
                .build();

        //Setup tile with TextArea to display Y axis output data
        textAreaY_axis = new TextArea();

        //Make the TextArea non editable
        textAreaY_axis.setEditable(false);

        /*Change the background and the font color of the TextArea
           and make the border of the TextArea transparent
         */
        textAreaY_axis.setStyle("-fx-control-inner-background: #2A2A2A; "
                + "-fx-text-inner-color: white;"
                + "-fx-text-box-border: transparent;");

        //Layout to contain the TextArea
        VBox textAreaVbox_Y = new VBox(textAreaY_axis);

        //Setup the tile to display Y axis output
        var textAreaTile_Y = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(350, 300)
                .textSize(TextSize.BIGGER)
                .title("Y axis value of joystick motion")
                .graphic(textAreaVbox_Y)
                .build();

        //Setup tile with TextArea to display Z axis output data
        textAreaZ_axis = new TextArea();

        //Make the TextArea non editable
        textAreaZ_axis.setEditable(false);

        /*Change the background and the font color of the TextArea
           and make the border of the TextArea transparent
         */
        textAreaZ_axis.setStyle("-fx-control-inner-background: #2A2A2A; "
                + "-fx-text-inner-color: white;"
                + "-fx-text-box-border: transparent;");

        //Layout to contain the TextArea
        VBox textAreaVbox_Z = new VBox(textAreaZ_axis);

        //Setup the tile to display Y axis output
        var textAreaTile_Z = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(350, 300)
                .textSize(TextSize.BIGGER)
                .title("Z axis value of joystick motion")
                .graphic(textAreaVbox_Z)
                .build();

        //Setup a tile with an exit button to end the application
        var exitButton = new Button("Exit");

        //Setup event handler for the exit button
        exitButton.setOnAction(e -> endApplication());

        //Setup the tile
        var exitTile = TileBuilder.create()
                .skinType(SkinType.CUSTOM)
                .prefSize(350, 300)
                .textSize(TextSize.BIGGER)
                .title("Quit the application")
                .graphic(exitButton)
                .roundedCorners(false)
                .build();

        //Add the tiles to VBoxes
        var tilesColumn1 = new VBox(clockTile, textAreaTile_X);
        tilesColumn1.setMinWidth(350);
        tilesColumn1.setSpacing(5);

        var tilesColumn2 = new VBox(choiceboxTile, textAreaTile_Y);
        tilesColumn2.setMinWidth(350);
        tilesColumn2.setSpacing(5);

        var tilesColumn3 = new VBox(exitTile, textAreaTile_Z);
        tilesColumn3.setMinWidth(350);
        tilesColumn3.setSpacing(5);

        //Add the VBoxes to the root layout, which is a HBox
        this.getChildren().add(tilesColumn1);
        this.getChildren().add(tilesColumn2);
        this.getChildren().add(tilesColumn3);
        this.setSpacing(5);
    }

    /*Setup thread to print the output from the Joystick program
      to the dashboard.
     */
    private void updateScreen(String line) {

        //Use the split method to parse the String
        String[] values = line.split(",");
        if (values.length != 3) {
            LOGGER.log(Level.SEVERE, "Unexpected results in updateScreen()");
            throw new IllegalStateException();
        }

        //Thread to update the screen
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                var timeStamp = new Date();
                if (theChoice.equals(choice1)) {
                    textAreaX_axis.setText("\n\nAt: " + timeStamp + "\n" + values[0]);
                    textAreaY_axis.setText("\n\nAt: " + timeStamp + "\n" + values[1]);
                    textAreaZ_axis.setText("\n\nAt: " + timeStamp + "\n" + values[2]);
                } else {
                    textAreaX_axis.appendText("\n\nAt: " + timeStamp + "\n" + values[0]);
                    textAreaY_axis.appendText("\n\nAt: " + timeStamp + "\n" + values[1]);
                    textAreaZ_axis.appendText("\n\nAt: " + timeStamp + "\n" + values[2]);
                }
            }
        });
    }

    //Setup a thread to get output from the ProcessBuilder Process
    private void getOutputThread() {

        //Initialize the variable that indicates when the thread must stop
        exit = false;

        //Create an anonymous subclass of Thread
        Thread thread = new Thread() {
            @Override
            public void run() {
                BufferedReader input = new BufferedReader(new InputStreamReader(theProcess.getInputStream()));

                //String to store output
                String line = null;
                int lineCount = 0;
                try {
                    while ((line = input.readLine()) != null && exit != true) {

                        //Increment the line counter
                        lineCount += 1;

                        //Normalize the line of input
                        String theLine = Normalizer.normalize(line, Form.NFKC);
                        System.out.println("In getOutputThread(), theLine is: " + theLine);

                        /*Validate that the line of input only contain letters,
                          numbers, _, :, commas and white spaces.
                          NOTE: this pattern only occurs after the first 5 lines
                          of output
                         */
                        Pattern pattern = Pattern.compile("^[A-Za-z0-9_:,\\s+]*$");

                        /*Search for the regex pattern in the normalized
                          line of input.
                         */
                        Matcher matcher = pattern.matcher(theLine);

                        //If the regex pattern isn't found
                        if (lineCount > 5 && !(matcher.find())) {
                            LOGGER.log(Level.SEVERE, "Output from joystick in incorrect format");
                            throw new IllegalStateException();

                        } else {
                            if (lineCount > 5) {
                                updateScreen(theLine);
                            }
                        }
                    }

                    //If exit equals true, close the BufferedReader
                    if (exit == true) {
                        input.close();
                    }

                } catch (IOException ex) {
                    Logger.getLogger(FXScreen.class.getName()).log(Level.SEVERE, null, ex);

                }
            }
        };

        thread.start();
    }

    //Stop the threads and close the application
    private void endApplication() {
        this.exit = true;
        Platform.exit();
    }

}
