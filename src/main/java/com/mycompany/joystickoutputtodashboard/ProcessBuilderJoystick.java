package com.mycompany.joystickoutputtodashboard;

import java.io.IOException;
/**
 *
 * @author Carlton Davis
 */

/* ProcessBuilder class to get output from C program
   that controls the Joystick circuit.
*/
public class ProcessBuilderJoystick {
    
    private final ProcessBuilder processBuilder;
     
    /*Constructor for the class, takes a string
       containing the C executable with the full path.
    */ 
    public ProcessBuilderJoystick(String theApp) {
        this.processBuilder = new ProcessBuilder();
        this.processBuilder.command(theApp);
     }
    
    
    //Return the process associated with the ProcessBuilder
    public Process getTheProcess() throws IOException {
       //Start the process
        var process = this.processBuilder.start(); 
        return process;
    }
}